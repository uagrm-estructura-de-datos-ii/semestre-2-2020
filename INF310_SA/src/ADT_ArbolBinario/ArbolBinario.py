from Nodo import Nodo

class ArbolBinario :
    __raiz__ = None
    __n__ = 0
    
    def __init__(self, dato):
        self.__raiz__ = Nodo(dato)
        self.__n__ = 0
    
    def insertar(self, padre, hijo) :
        dirPadre = self.__buscar(self.__raiz__, padre)
        if dirPadre is not None :
            dirHijo = self.__buscar(self.__raiz__, hijo)
            if dirHijo is None :
                pos = dirPadre.posHijoLibre()
                if pos != -1 :
                    dirPadre.setHijo(Nodo(hijo), pos)
                    self.__n__ = self.__n__ + 1
            
                else :
                    print("El padre esta completo y no puede aceptar un nuevo hijo")
            
            else :
                print("El hijo existe en el arbol")
        
        else : 
            print("El padre existe en el arbol")
    
    def __buscar(self, nodo, x):
        if nodo is None:
            return None
        if nodo.getDato() == x :
            return nodo
        h1 = self.__buscar(nodo.getHijo(1), x)
        if h1 is not None:
            return h1
        return self.__buscar(nodo.getHijo(2), x)
    
    def inOrden(self):
        print("INORDEN")
        if self.__raiz__ is None:
            print("Arbol vacio")
        else:
            self.__inOrden1(self.__raiz__)

    def __inOrden1(self, nodo):
        if nodo is not None:
            self.__inOrden1(nodo.getHijo(1))
            print("[ " + str(nodo.getDato()) + " ]")
            self.__inOrden1(nodo.getHijo(2))
            
    def cantidadHojas(self):
        return self.__cantidadHojas(self.__raiz__)
    
    def __cantidadHojas(self, nodo):
        if nodo is None :
            return 0
        if nodo.cantidadHijo() == 0:
            return 1        
        return self.__cantidadHojas(nodo.getHijo(1)) + self.__cantidadHojas(nodo.getHijo(2))  
    
    def cantidadHojasImp(self):
        return self.__cantidadHojasImp(self.__raiz__)
    
    def __cantidadHojasImp(self, nodo):
        if nodo is None :
            return 0
        if nodo.cantidadHijo() == 0:
            if nodo.getDato() % 2 == 1 :
                return 1        
            else:
                return 0
        return self.__cantidadHojasImp(nodo.getHijo(1)) + self.__cantidadHojasImp(nodo.getHijo(2)) 
    
    def cantidadHijoTriangulo(self):
        return self.__cantidadHijoTriangulo(self.__raiz__)
    
    def __cantidadHijoTriangulo(self, nodo):
        if nodo is None :
            return 0
        if nodo.cantidadHijo() == 0:
            return 0
        if nodo.cantidadHijo() == 2:
            if ((nodo.getHijo(1).cantidadHijo() == 0) and (nodo.getHijo(2).cantidadHijo() == 0)):
                return 1
            
        return self.__cantidadHijoTriangulo(nodo.getHijo(1)) + self.__cantidadHijoTriangulo(nodo.getHijo(2)) 