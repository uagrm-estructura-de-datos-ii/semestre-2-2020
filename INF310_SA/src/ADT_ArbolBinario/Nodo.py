class Nodo:
    __dato__ = 0
    __h1__ = None
    __h2__ = None
    
    def __init__(self, dato):
        self.__dato__ = dato
        self.__h1__ = None
        self.__h2__ = None
        
    def setDato(self, dato):
        self.__dato__ = dato
        
    def setHijo(self, hijo, n):
        if n == 1 :
            self.__h1__ = hijo
        if n == 2 :
            self.__h2__ = hijo
        else :
            print("El indice de hijo que desea settear es erroneo.")
    
    def getDato(self):
        return self.__dato__
    
    def getHijo(self, n):
        if n == 1 :
            return self.__h1__
        if n == 2 :
            return self.__h2__
        print("Indice de hijo incorrecto.")
        return None
    
    def cantidadHijo(self):
        a = 0
        if self.__h1__ is not None :
            a = a + 1
        if self.__h2__ is not None :
            a = a + 1
        return a;
            
    
    def posHijoLibre(self):
        if self.__h1__ is None :
            return 1
        if self.__h2__ is None :
            return 2
        return -1