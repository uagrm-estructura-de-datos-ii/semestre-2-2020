class Ave:
    def __init__(self):
        self.__especie = None
        self.__edad = None
        self.__region = None
        
    def __init__(self, especie, edad, region):
        self.__especie = especie
        self.__edad = edad
        self.__region = region
        
    def getEspecie(self):
        return self.__especie
    
    def getEdad(self):
        return self.__edad
    
    def getRegion(self):
        return self.__region
    
    def imprimir(self):
        s = '----------------------------'+' \n '
        s = s + ' Especie : '+ self.__especie+' \n '
        s = s + ' Edad : '+ str(self.__edad)+' \n '
        s = s + ' Region : '+ self.__region+' \n '
        s = s + '----------------------------'+' \n '
        return s
        
