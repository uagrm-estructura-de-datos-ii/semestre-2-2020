'''
================================================================
Importar los modulos que son necesarios para la implementacion del arbol binario el cual tenga la siguientes caracteristicas:

1.- Implementar el metodo Add que permita clasificar las aves segun su especie, edad y zona geografica donde habitan.

2.- Crear un metodo que permita listar en forma clasificada cada una de las especies de aves segun su edad y zona geograficas donde habitan.

================================================================
[
'aguila' => [
    1 => 
        ['Bolivia', 'Argentina', 'SurAmerica'],
    2 =>
        ['Bolivia'] 
    ], 
'Loro' => [
    ] 
]

'''


# Library Local
from dNodo import TreeNode
from ave import Ave
#. class what manager the structure treenode        
class MyTree:
    root = TreeNode # None
    
    def __init__(self):
        self.__root = None
#. Constructor of class MyTree 
    ''' Clase MyTree '''    
    ''' Arguments of class MyTree
    Note:
        Esta es una clase que permite manejar nodos jerarquicos
    Args:

    Returns:

    '''
    def setRoot(self,link):
        self.__root = link;
    def getRoot(self):
        return self.__root;


#. definition of method of class MyTree
    def addNode(self, especie, edad, region):
        self.__addNode(self.__root, especie, edad, region)

    def __addNode(self, node, especie, edad, region): 
        ''' Methos create node, parameter node and value.
        Args:
        node estructure of type node class.
        Returns:
        Node with value of elements, left,rigth and data.
        '''
        value = Ave(especie, edad, region)
        if(node==None):
            self.setRoot(TreeNode(value))
        else:
            if(value.getEspecie() < node.getData().getEspecie()):
                if(node.getLeft()==None):                
                    node.setLeft(TreeNode(value))
                else:
                    self.__addNode(node.getLeft(), especie, edad, region)
            elif(value.getEspecie() > node.getData().getEspecie()):
                if(node.getRight()==None):
                    node.setRight(TreeNode(value))
                else:
                    self.__addNode(node.getRight(), especie, edad, region)
            else:
                if(value.getEdad() < node.getData().getEdad()):
                    if(node.getLeft()==None):                
                        node.setLeft(TreeNode(value))
                    else:
                        self.__addNode(node.getLeft(), especie, edad, region)
                elif(value.getEdad() > node.getData().getEdad()):
                    if(node.getRight()==None):
                        node.setRight(TreeNode(value))
                    else:
                        self.__addNode(node.getRight(), especie, edad, region)
                else:
                    if(value.getRegion() < node.getData().getRegion()):
                        if(node.getLeft()==None):                
                            node.setLeft(TreeNode(value))
                        else:
                            self.__addNode(node.getLeft(), especie, edad, region)
                    elif(value.getRegion() > node.getData().getRegion()):
                        if(node.getRight()==None):
                            node.setRight(TreeNode(value))
                        else:
                            self.__addNode(node.getRight(), especie, edad, region)
                
                    
#. Method printing the data of class mytree
    def printInorder(self):
        self.__printInorder(self.__root)
   
    def __printInorder(self, node):
        if(node != None):
            self.__printInorder(node.getLeft())
            print(node.getData().imprimir())
            self.__printInorder(node.getRight())
            
    def clasificacion(self):
        clas = {}
        self.__clasificacion(self.__root, clas)
        return clas
    
    def __clasificacion(self, node, clas):
        if(node == None):
            return clas
        clas = self.__agregarAve(clas, node.getData())
        self.__clasificacion(node.getLeft(), clas)
        self.__clasificacion(node.getRight(), clas)
        return clas
        
    def __agregarAve(self, clas, ave):
        if(ave.getEspecie() in clas):
            edades = clas[ave.getEspecie()]   
            if(ave.getEdad() in edades):
                regiones = edades[ave.getEdad()]
                if(ave.getRegion() not in regiones):
                    regiones.append(ave.getRegion())
                edades[ave.getEdad()] = regiones
            else:
                edades[ave.getEdad()] = [ave.getRegion()]                
            clas[ave.getEspecie()] = edades       
        else:
            clas[ave.getEspecie()] = {}
        return clas
        