from treeBinary import MyTree

arbol = MyTree()
arbol.addNode('Loro', 1, 'SurAmerica')
arbol.addNode('Paloma', 1, 'SurAmerica')
arbol.addNode('Aguila', 2, 'NorteAmerica')

arbol.addNode('Loro', 2, 'SurAmerica')
arbol.addNode('Paloma', 3, 'SurAmerica')
arbol.addNode('Aguila', 1, 'NorteAmerica')

arbol.addNode('Loro', 4, 'SurAmerica')
arbol.addNode('Paloma', 0, 'SurAmerica')
arbol.addNode('Aguila', 5, 'NorteAmerica')

arbol.addNode('Loro', 4, 'Bolivia')
arbol.addNode('Paloma', 0, 'Argentina')
arbol.addNode('Aguila', 5, 'Brasil')

clasificacion = arbol.clasificacion()
print(clasificacion)
idEspecie = 1
for especie in clasificacion:
    print(str(idEspecie) + ' ' +especie)
    idEdad = 1
    for edad in clasificacion[especie]:
        print('  '+str(idEspecie)+'.'+str(idEspecie)+' Edad: '+str(edad))
        idRegion = 1
        for region in clasificacion[especie][edad]:
            print('    '+str(idEspecie)+'.'+str(idEspecie)+'.'+str(idRegion)+' '+region)
            idRegion = idRegion+1
        idEdad = idEdad + 1
    idEspecie = idEspecie + 1
