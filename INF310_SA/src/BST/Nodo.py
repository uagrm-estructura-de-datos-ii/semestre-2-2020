class Nodo:
    __data__ = 0
    __hi__ = None
    __hd__ = None

    def __init__(self, data):
        self.__data__ = data
        self.__hi__ = None
        self.__hd__ = None

    def setData(self, data):
        self.__data__ = data;

    def setHI(self, hi):
        self.__hi__ = hi

    def setHD(self, hd):
        self.__hd__ = hd

    def getData(self):
        return self.__data__

    def getHI(self):
        return self.__hi__

    def getHD(self):
        return self.__hd__

    def cantidadHijos(self):
        a = 0
        if self.__hi__ is not None:
            a = a + 1
        if self.__hd__ is not None:
            a = a + 1
        return a

    def esHoja(self):
        if self.cantidadHijos() == 0:
            return True
        return False