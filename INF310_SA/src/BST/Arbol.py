from BST.Nodo import Nodo

class Arbol:
    __raiz__ = None
    __n__ = 0
    
    def __init__(self):
        self.__raiz__ = None
        self.__n__ = 0
        
    def insertar(self, x):
        if self.__raiz__ is None:
            self.__raiz__ = Nodo(x)            
        else:
            azul = None
            rojo = self.__raiz__
            while rojo is not None:
                azul = rojo
                if x < rojo.getData():
                    rojo = rojo.getHI()
                else:
                    if x > rojo.getData():
                        rojo = rojo.getHD()
                    else:
                        print("El elemento a introducir ya existe en el arbol")
                        return
            if x < azul.getData():
                hijo = Nodo(x)
                azul.setHI(hijo)
            else:
                hijo = Nodo(x)
                azul.setHD(hijo)
        self.__n__ = self.__n__ + 1
    
    def inOrden(self):
        print("IN-ORDEN")
        if self.__raiz__ is None:
            print("El arbol esta vacio")
        else:
            self.__inOrden1(self.__raiz__)
                
    def __inOrden1(self, nodo):
        if(nodo is not None) :
            self.__inOrden1(nodo.getHI())
            print("[ "+ str(nodo.getData()) + " ] ")
            self.__inOrden1(nodo.getHD())
            
    def sumarNodo(self):
        return self.__sumarNodo1(self.__raiz__)                
        
    def __sumarNodo1(self, nodo):
        if nodo is None:
            return 0
        if nodo.esHoja():
            return nodo.getData()
        return self.__sumarNodo1(nodo.getHI()) + self.__sumarNodo1(nodo.getHD()) +nodo.getData()
    
    def sumarNodoMenorIgual(self, x):
        return self.__sumarNodoMenorIgual1(self.__raiz__, x)                
        
    def __sumarNodoMenorIgual1(self, nodo, x):
        if nodo is None:
            return 0
        if nodo.esHoja():
            if nodo.getData() <= x :
                return nodo.getData()
            else:
                return 0
        shi = self.__sumarNodoMenorIgual1(nodo.getHI(), x)
        shd = self.__sumarNodoMenorIgual1(nodo.getHD(), x)
        yo = 0
        if nodo.getData() <= x:
            yo = nodo.getData()
        return shi + shd + yo
    
    def __podar(self, nodo, x):
        if nodo is None:
            return None
        if nodo.esHoja():
            if nodo.getData() == x :
                return None
            return nodo
        if nodo.getData() == x:
            return None
        nodo.setHI(self.__podar(nodo.getHI(), x))
        nodo.setHD(self.__podar(nodo.getHD(), x))
        return nodo
        
    
    def podar(self, x):
        self.__raiz__ = self.__podar(self.__raiz__, x)
        
    def eliminarIncompleto(self, x):
        self.__raiz__ = self.__eliminarIncompleto(self.__raiz__, x)
    
    def __eliminarIncompleto(self, nodo, x):
        if(nodo is None):
            return None
        print('rec : ' + str(nodo.getData()))
        if(nodo.esHoja()):
            return nodo
        
        if(nodo.getData() == x):
            if(nodo.cantidadHijos() == 1):
                if(nodo.getHI() is not None):
                    nodo = nodo.getHI()
                else :
                    nodo = nodo.getHD()
                return nodo
        if(nodo.getData() > x):
            nodo.setHI( self.__eliminarIncompleto(nodo.getHI(), x) )
        if(nodo.getData() < x):    
            nodo.setHD( self.__eliminarIncompleto(nodo.getHD(), x) )
        return nodo
    
    def promedioIncompleto(self):
        suma = self.__sumaIncompleto(self.__raiz__)
        cantidad = self.__cantidadIncompleto(self.__raiz__)
        return suma/cantidad
    
    def __sumaIncompleto(self, nodo):
        if(nodo is None):
            return 0
        if(nodo.esHoja()):
            return 0
        s = 0
        if(nodo.cantidadHijos() == 1):
            s = nodo.getData()
        s = s + self.__sumaIncompleto(nodo.getHI())
        s = s + self.__sumaIncompleto(nodo.getHD())
        return s
    
    def __cantidadIncompleto(self, nodo):
        if(nodo is None):
            return 0
        if(nodo.esHoja()):
            return 0
        c = 0
        if(nodo.cantidadHijos() == 1):
            c = 1
        c = c + self.__cantidadIncompleto(nodo.getHI())
        c = c + self.__cantidadIncompleto(nodo.getHD())
        return c
        
        