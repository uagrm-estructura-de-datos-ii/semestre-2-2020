/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ADTLotto3;


/**
 *
 * @author Ronaldo Rivero
 */
public class Lista {
    
    private Nodo L;
    private int n;

    public Lista() {
        this.L = null;
        this.n = 0;
    }
    
    public void add(int x) {
        if(isEmpty()) {
            L = new Nodo(x);
            n = 1;
        } else {
            Nodo ant = null;
            Nodo p = L;
            while(p != null && x >= p.getData()) {
                if(x == p.getData()) {
                    System.err.println("El elemento " +x+ " ya existe.");
                    return;
                }
                ant = p;
                p = p.siguienteNodo();
            }
            
            if(ant == null) { // significa que x debe ir al inicio
                Nodo nuevo = new Nodo(x);
                L = nuevo;
                nuevo.setLink(p);
                n++;
            }
            
            if(p != null && ant != null) { //significa que x debe ir entre medio
                Nodo nuevo = new Nodo(x);
                ant.setLink(nuevo);
                nuevo.setLink(p);
                n++;
            }
            
            if(p == null && ant != null) { // significa que x debe ir al final
                Nodo nuevo = new Nodo(x);
                ant.setLink(nuevo);
                n++; 
            }
        }
    } 
    
    public boolean isEmpty() {
        return L == null;
    }
    
    public boolean exists(int x) {
        Nodo p = L;
        while(p != null) {
            if(p.getData() == x) {
                return true;
            }
            p = p.siguienteNodo();
        }
        
        return false;
    }
    
    public void resetearAciertos() {
        if(L == null)
            return;
        Nodo aux = L;
        while(aux != null) {
            aux.setAciertos(0);
            aux = aux.siguienteNodo();
        }
    }
    
    public Nodo getTicket(int x) {
        Nodo p = L;
        while(p != null) {
            if(p.getData() == x) {
                return p;
            }
            p = p.siguienteNodo();
        }
        
        return null;
    }
    
    public void aumentarAcierto(int x) {
        Nodo p = L;
        while(p != null) {
            if(p.getData() == x) {
                int ac = p.getAciertos();
                p.setAciertos(ac+1);
            }
            p = p.siguienteNodo();
        }
    }
    
    public void shoot(int x) {
        if(L == null)
            return;        
        int dist = distanciaMasCercana(x);
        Nodo aux = L;
        Nodo ant = null;
        while(aux != null) {
            int d = Math.abs(aux.getData() - x);
            if(d == dist) {
                break;
            }
            ant = aux;
            aux = aux.getLink();
        }
        int ticket = aux.getAciertos() + 1;
        aux.setAciertos( ticket );
        if(ticket == 2) {
            n--;
            if(ant == null) {
                L = L.getLink();
                return;
            }
            if(aux.getLink() == null) {
                ant.setLink(null);                
                return;
            }
            ant.setLink(aux.getLink());
        }
    }
    
    private int distanciaMasCercana(int x) {
        if(L == null)
            return -1;
        int mDist = Math.abs(L.getData() - x);
        Nodo aux = L;
        while(aux != null) {
            int d = Math.abs(aux.getData() - x);
            if(d < mDist)
                mDist = d;
            aux = aux.getLink();
        }
        return mDist;
    }
    
    public int length() {
        return n;
    }
    /**
     * La función get devuelve el elemento en la posición dada
     * si la posicion no es correcta dentro del rango 0 .. n-1
     * o la lista se encuentra vacía
     * está función retorna -1
     * @param pos
     * @return 
     */
    public int get(int pos) {
        Nodo p = L;
        int i = 0;
        while(p != null) {
            if(i == pos) {
                return p.getData();
            }
            p = p.siguienteNodo();
            i++;
        }
        return -1;
    }
    
    
    
    public void delete(int pos) {
        if(pos >= 0 && pos < n) {
            if(pos == 0) {
                L = L.getLink();               
            } else {
                int i = 0;
                Nodo rojo = L;
                
                Nodo azul = null;               
                while(i < pos) {
                    azul = rojo;
                    rojo = rojo.getLink();
                    i++;
                }
                azul.setLink(rojo.getLink());               
            }
            n--;            
        } else {
            System.err.println("La posicion enviada es invalida.");
        }
    }
    
    public void deleteX(int x) {
        if(!isEmpty()) {
            Nodo azul = null;
            Nodo rojo = L;
            while(rojo != null) {
                if(rojo.getData() == x) {
                    break;
                }
                azul = rojo;
                rojo = rojo.getLink();
            }
            
            if(azul == null && rojo != null) {
                L = L.getLink();
                n--;
            } else if(azul != null && rojo != null) {
                azul.setLink(rojo.getLink());
                n--;
            } else if(azul != null && rojo == null) {
                System.err.println("El elemento X a eliminar no existe.");
            }
        }
    }
    
    public void deleteXR(int x) {
        L = deleteXR(L, x);
    }
    
    private Nodo deleteXR(Nodo p, int x) {
        if(p == null){
            System.err.println("El elemento X a eliminar no existe.");
            return null;
        }           
        if(p.getData() == x)
            return p.getLink();
        p.setLink( deleteXR(p.getLink(), x) );
        return p;
    }

    @Override
    public String toString() {
        String s = "[ ";
        Nodo p = L;
        while(p != null) {
            s = s + p.getData() + " | ";
            p = p.siguienteNodo();
        }        
        return s+" ]";
    }
}
