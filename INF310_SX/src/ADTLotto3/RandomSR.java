/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ADTLotto3;

/**
 *
 * @author Ronaldo Rivero
 */
public class RandomSR {
    
    int v[];
    int tope;
    
    /**
     * Contruye el objeto Random Sin Repetir
     * desde 1 hasta n
     * @param n 
     */
    public RandomSR(int n) {
        this.tope = n;
        v = new int[n];
        for(int i = 1; i <= n; i++){
            v[i-1] = i;           
        }
       
    }
    /**
     * Genera un numero al azar sin repetir
     * en caso se acabe los numeros de azar
     * esta funcion retornara -1
     * @return 
     */
    public int getAzar() {
        if(tope == 0) {
            return -1;
        }
        int r = (int)(Math.random()*tope);
        System.err.println(r);
        int azar = v[r];
        actualizar(r);
        tope--;
        return azar;
    }
    
    private void actualizar(int pos) {
        for(int i = pos; i < tope-1; i++) {
            v[i] = v[i+1];
        }
    }
}
