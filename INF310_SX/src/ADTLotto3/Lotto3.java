/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ADTLotto3;

/**
 *
 * @author Ronaldo Rivero
 */
public class Lotto3 {
    private Lista lista;
    private RandomSR random;
    
    public Lotto3(int n) {
        random = new RandomSR(10);
        lista = new Lista();
        for(int i=0; i<n; i++){
            int r = random.getAzar();
            if(r != -1)
                lista.add(r);
        }
    }
    
    public void iniciar() {
        lista.resetearAciertos();
    }
    
    public int registrar(int b) {
        if(lista.exists(b) == false)
            return -2;
        lista.aumentarAcierto(b);
        int acierto = lista.getTicket(b).getAciertos();
        if(acierto == 3)
            return b;
        return -1;
    }

    public void shoot(int x) {
        lista.shoot(x);
    }
    
    @Override
    public String toString() {
        String s = "[ ";
        int c = lista.length();
        for(int i = 0; i < c; i++) {
            int ticket = lista.get(i);
            int acierto = lista.getTicket(ticket).getAciertos();
            s = s + ticket+ " => " +acierto + " | ";
        }        
        return s+" ]";
    }
    
}
