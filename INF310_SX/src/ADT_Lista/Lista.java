/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ADT_Lista;

/**
 *
 * @author Ronaldo Rivero
 */
public class Lista {
    
    private Nodo L;
    private int n;
    private int centinela;

    public Lista() {
        this.L = null;
        this.n = 0;
        this.centinela = 0;
    }
    
    public int length() {
        return n;
    }

    public void shift(int k) {
        if(centinela + k >=0 && centinela+k <= length()) {
            centinela = centinela + k;
        }
    }
    
    public void addE(int x) {
        if(exist(x))
            return;
        
        if(L == null) {
            if(centinela == 0) {
                L = new Nodo(x); n++;
            }
            return;
        }
        Nodo aux = L;
        Nodo ant = null;
        int pos = 0; 
        while(pos != centinela) {
            ant = aux;
            aux = aux.getLink();
            pos++;
        }
        Nodo p = new Nodo(x); n++;
        if(ant == null) {
            p.setLink(L);
            L = p; return;
        }
        if(aux == null) {
            ant.setLink(p); return;
        }
        ant.setLink(p);
        p.setLink(aux);
    }
    
    public void add(int x) {
        if(isEmpty()) {
            L = new Nodo(x);
            n = 1;
        } else {
            Nodo ant = null;
            Nodo p = L;
            while(p != null && x >= p.getData()) {
                if(x == p.getData()) {
                    System.err.println("El elemento " +x+ " ya existe.");
                    return;
                }
                ant = p;
                p = p.siguienteNodo();
            }
            
            if(ant == null) { // significa que x debe ir al inicio
                Nodo nuevo = new Nodo(x);
                L = nuevo;
                nuevo.setLink(p);
                n++;
            }
            
            if(p != null && ant != null) { //significa que x debe ir entre medio
                Nodo nuevo = new Nodo(x);
                ant.setLink(nuevo);
                nuevo.setLink(p);
                n++;
            }
            
            if(p == null && ant != null) { // significa que x debe ir al final
                Nodo nuevo = new Nodo(x);
                ant.setLink(nuevo);
                n++; 
            }
        }
    }
    
    
    public boolean isEmpty() {
        return L == null;
    }
    
    public boolean exists(int x) {
        Nodo p = L;
        while(p != null) {
            if(p.getData() == x) {
                return true;
            }
            p = p.siguienteNodo();
        }
        
        return false;
    }
    
    private boolean existe(Nodo p, int x) {
        if(p == null)
            return false;
        if(p.getData() == x)
            return true;
        
        boolean ex = existe(p.siguienteNodo(), x);
        if(ex == true)
            return true;
        return ex;
    }
    /**
     * La función get devuelve el elemento en la posición dada
     * si la posicion no es correcta dentro del rango 0 .. n-1
     * o la lista se encuentra vacía
     * está función retorna -1
     * @param pos
     * @return 
     */
    public int get(int pos) {
        Nodo p = L;
        int i = 0;
        while(p != null) {
            if(i == pos) {
                return p.getData();
            }
            p = p.siguienteNodo();
            i++;
        }
        return -1;
    }
    
    public void delE(int x) {
        if(L == null)
            return;
        Nodo aux = L;
        Nodo ant = null;
        while(aux != null) {
            if(aux.getData() == x)
                break;
            ant = aux;
            aux = aux.getLink();
        }
        if(aux == null)
            return;
        if(aux.getLink() == null) {
            if(ant == null) {
                L = null;
            } else {
                ant.setLink(null); 
            }            
            return;
        }
        if(ant == null) {
            L = L.getLink();
        } else {
            ant.setLink(aux.getLink());
        }
        aux = L;
        ant = null;
        while(aux != null) {
            ant = aux;
            aux = aux.getLink();
        }
        Nodo p = new Nodo(x);
        ant.setLink(p);
    }
    
    public void del(int k) {
        if(L == null)
            return;
        if(k == 0) {
            L = L.getLink();
        }
        Nodo aux = L;
        Nodo ant = null;
        int pos = 0;
        while(aux != null) {
            if(pos == k) {
                ant.setLink(aux.getLink());
                return;
            }
            ant = aux;
            aux = aux.siguienteNodo();
            pos++;
        }
    }
    
    public boolean exist(int x) {
        return exist(L, x);
    }
    
    private boolean exist(Nodo p, int x) {
        if(p == null)
            return false;
        if(p.getData() == x)
            return true;
        return exist(p.siguienteNodo(), x);
    }
    
    public void delete(int pos) {
        if(pos >= 0 && pos < n) {
            if(pos == 0) {
                L = L.getLink();               
            } else {
                int i = 0;
                Nodo rojo = L;
                
                Nodo azul = null;               
                while(i < pos) {
                    azul = rojo;
                    rojo = rojo.getLink();
                    i++;
                }
                azul.setLink(rojo.getLink());               
            }
            n--;            
        } else {
            System.err.println("La posicion enviada es invalida.");
        }
    }
    
    public void deleteX(int x) {
        if(!isEmpty()) {
            Nodo azul = null;
            Nodo rojo = L;
            while(rojo != null) {
                if(rojo.getData() == x) {
                    break;
                }
                azul = rojo;
                rojo = rojo.getLink();
            }
            
            if(azul == null && rojo != null) {
                L = L.getLink();
                n--;
            } else if(azul != null && rojo != null) {
                azul.setLink(rojo.getLink());
                n--;
            } else if(azul != null && rojo == null) {
                System.err.println("El elemento X a eliminar no existe.");
            }
        }
    }
    
    public void deleteXR(int x) {
        L = deleteXR(L, x);
    }
    
    private Nodo deleteXR(Nodo p, int x) {
        if(p == null){
            System.err.println("El elemento X a eliminar no existe.");
            return null;
        }           
        if(p.getData() == x)
            return p.getLink();
        p.setLink( deleteXR(p.getLink(), x) );
        return p;
    }

    @Override
    public String toString() {
        String s = "[ ";
        Nodo p = L;
        while(p != null) {
            s = s + p.getData() + " | ";
            p = p.siguienteNodo();
        }        
        return s+" ]";
    }
}
