/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ADT_Lista;

/**
 *
 * @author Ronaldo Rivero
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        preguntaAddE();
        /*Lista L = new Lista();
        L.add(8);
        System.out.println(L);
        L.delE(8);
        System.out.println(L);
        L.add(3);        
        L.add(9);        
        L.add(11);
        System.out.println(L);
        L.delE(9);
        System.out.println(L);
        L.delE(9);                
        System.out.println(L);
        
        //System.out.println("existe 15 ? " + L.exist(15));
        //System.out.println("existe 8 ? " + L.exist(8));
        
        
        /*
        System.out.println(L);
        
        L.add(15);
        System.out.println(L);
        
        L.add(25);
        System.out.println(L);
        
        L.add(12);
        System.out.println(L);
        
        L.add(5);
        System.out.println(L);
        
        L.add(15);
        System.out.println(L);
        
        System.out.println("ELIMINANDO XR = 5");//5 - 12 - 25
        L.deleteXR(100);
        System.out.println(L);
        
        /*
        System.out.println("ELIMINANDO X = 200");//5 - 12 - 25
        L.deleteX(200);
        System.out.println(L);
        */
        /*
        System.out.println("ELIMINANDO LA POSICION 4");
        L.delete(10);
        System.out.println(L);
        */
        
        /*
        System.out.println("Existe el 25 : " + L.exists(25));
        System.out.println("Existe el 35 : " + L.exists(35));
        
        System.out.println("get(3) : " + L.get(3));
        System.out.println("get(5) : " + L.get(5));
        */
    }
    
    public static void preguntaAddE() {
        Lista lista = new Lista();
        lista.addE(8);
        lista.addE(6);
        System.out.println(lista);
        lista.shift(2);
        lista.addE(3);
        System.out.println(lista);
        lista.shift(-1);
        lista.addE(3);
        lista.addE(9);
        System.out.println(lista);
        lista.shift(-2);
        lista.shift(4);
    }
    
}
