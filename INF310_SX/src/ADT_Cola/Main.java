/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ADT_Cola;

/**
 *
 * @author Ronaldo Rivero
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Cola c = new Cola();
        c.add(9);
        c.add(8);
        c.add(6);
        c.add(4);
        c.add(5);
        c.add(9);
        System.out.println(c);
        System.out.println("Sacar " + c.pop());
        System.out.println(c);
        c.add(10);
        System.out.println(c);
    }
    
}
