/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ADT_Cola;

/**
 *
 * @author Ronaldo Rivero
 */
public class Cola {
    private Nodo L;
    
    public Cola() {
        L = null;
    }
    
    public void add(int x) {
        if(L == null) {
            L = new Nodo(x);
            return;
        }
        if(x % 2 == 0) {
            Nodo aux = L;
            Nodo ant = null;
            while(aux != null && aux.getData() % 2 == 0) {
                ant = aux;
                aux = aux.getLink();
            }
            Nodo p = new Nodo(x);
            if(ant == null) {
                p.setLink(L);
                L = p;
                return;
            }
            if(aux == null) {
                ant.setLink(p);
                return;
            }            
            ant.setLink(p);
            p.setLink(aux);
        } else {
            Nodo aux = L;
            Nodo ant = null;
            while(aux != null) {
                ant = aux;
                aux = aux.getLink();
            }
            Nodo p = new Nodo(x);
            ant.setLink(p);
            p.setLink(aux);
        }        
    }
    
    public int pop() {
        if(L == null)
            return -1;
        int x = L.getData();
        L = L.getLink();
        return x;
    }
    
    @Override
    public String toString() {
        String s = "[ ";
        Nodo p = L;
        while(p != null) {
            s = s + p.getData() + " | ";
            p = p.siguienteNodo();
        }        
        return s+" ]";
    }
}
