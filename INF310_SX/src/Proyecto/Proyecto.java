/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proyecto;

import Proyecto.Lista.Lista;
import Proyecto.Lista.Nodo;
import java.util.List;

/**
 *
 * @author Ronaldo Rivero
 */
public class Proyecto {

    static Lista archivos;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        archivos = new Lista();
        String path = "D:\\Prueba";
        iniciar(path);
        System.out.println(archivos);
        Nodo nodo = archivos.get(archivos.size()-1);
        Comando.del(nodo.path, nodo.nombre, nodo.formato);              
    }
    
    public static void iniciar(String path) {
        List<String> lines = Utilidad.limpiar(Comando.getDir(path));
        for (String line : lines) {            
            if(Utilidad.isCarpeta(line)) {
                String nombreCarpeta = Utilidad.getNombreCarpeta(line);
                iniciar(path+Comando.PAR+nombreCarpeta);
            } else {
                String nombreArchivo = Utilidad.getNombreArchivo(line);
                String formatoArchivo = Utilidad.getFormatoArchivo(line);                
                archivos.add(nombreArchivo, formatoArchivo, 0, path);
            }
        }
    }
    
}
