/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proyecto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ronaldo Rivero
 */
public class Utilidad {
    
    public static List<String> limpiar(List<String> lines) {
        if(lines.size() > 0) {
            List<String> a = new ArrayList<>();
            int i = 7;
            while(i < lines.size()-2) {
                a.add(lines.get(i));
                i++;
            }
            return a;
        }
        return lines;
    }
    
    public static boolean isCarpeta(String s) {
        return s.contains("<DIR>");
    }
    
    public static String getNombreCarpeta(String s) {
        int pos = s.indexOf("<DIR>");
        if(pos != -1) {
            return s.substring(pos+15);
        }
        return "";
    }
    
    public static String getNombreArchivo(String s) {
        int pos = s.indexOf(":");
        if(pos != -1) {
            int posPunto = s.lastIndexOf(".");
            return s.substring(pos+22, posPunto);
        }
        return "";
    }
    
    public static String getFormatoArchivo(String s) {
        int pos = s.lastIndexOf(".");
        if(pos != -1) {           
            return s.substring(pos);
        }
        return "";
    }
}
