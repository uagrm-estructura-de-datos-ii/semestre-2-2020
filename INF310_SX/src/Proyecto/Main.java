/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proyecto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 *
 * @author Ronaldo Rivero
 */
public class Main {

    static String path = "D:\\Prueba";
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {       
        DIR(path);
    }
    
    public static void DIR(String path) {
        try {
            String app = "cmd /c dir "+path;
            Process process = Runtime.getRuntime().exec(app);
            InputStream inputstream = process.getInputStream();
            BufferedReader input = new BufferedReader(new InputStreamReader(inputstream)); 
            
            PARSE(input);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void PARSE(BufferedReader input) {
        try {
            input.readLine();
            input.readLine();
            input.readLine();
            input.readLine();
            input.readLine();
            input.readLine();
            input.readLine();            
            while(input.read() > 0) {
                String row = input.readLine();  
                int p = row.indexOf("<DIR>");                
                String dir = path;
                String type = "";
                String weigth = "";
                String name = "";  
                String format = "";
                if(p != -1) {
                    type = "CARPETA";
                    name = row.substring(p+15, row.length());
                    path = path+"\\"+name;
                    DIR(path);
                } else {
                    type = "ARCHIVO";
                    row = deleteDateTime(row);
                    weigth = getWeigth(row);
                    row = deleteWeigth(row, weigth);
                    name = getName(row);
                    format = getFormat(row);
                    
                }
                String data = "DIR:"+dir+"\n";
                data = data+"TIPO:" + type + "\n";
                data = data+ "NOMBRE:" + name + "\n";
                data = data+ "PESO:" + weigth + "\n";
                data = data+ "FORMATO:" + format + "\n";
                System.out.println(data);
            }
            
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
    public static String deleteDateTime(String s) {
        int p = s.indexOf(":");
        if(p != -1) {
            String a = s.substring(p, s.length());
            p = a.indexOf(" ");
            if(p != -1) {
                return a.substring(p, a.length());
            }            
        }
        return s;
    };
    
    public static String deleteWeigth(String s, String weigth) {
        int p = s.indexOf(weigth);
        if(p != -1) {
            return s.substring(p+1, s.length());
        }
        return s;
    }
    
    public static String getWeigth(String s) {
        String a = "";
        boolean b = false;
        int i = 0;
        while(i < s.length() && b == false) {
            char c = s.charAt(i);
            if(c != 32 && b == false) {
                a = a + c;
            } else if(a.length() > 0) {
                b = true;
            }            
            i++;
        }
        return a;
    }
    
    public static String getName(String s) {
        int p = s.lastIndexOf(".");
        if(p != -1) {
            return s.substring(1, p);
        }
        return s;
    }
    
    public static String getFormat(String s) {
        int p = s.lastIndexOf(".");
        if(p != -1) {
            return s.substring(p+1, s.length());
        }
        return s;
    }
    
}
