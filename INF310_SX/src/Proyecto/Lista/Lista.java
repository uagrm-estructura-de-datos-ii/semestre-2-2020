/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proyecto.Lista;

import Proyecto.Lista.*;

/**
 *
 * @author Ronaldo Rivero
 */
public class Lista {
    
    private Nodo L;
    private int n;

    public Lista() {
        this.L = null;
        this.n = 0;
    }
    
    public void add(String nombre, String formato, int tamano, String path) {
        if(isEmpty()) {
            L = new Nodo(nombre, formato, tamano, path);
            n = 1;
        } else {
            Nodo ant = null;
            Nodo p = L;
            while(p != null) {
                ant = p;
                p = p.link;
            }
            Nodo nuevo = new Nodo(nombre, formato, tamano, path);
            ant.link = nuevo;
            n++; 
        }
    }
    
    
    public boolean isEmpty() {
        return L == null;
    }
    
    /**
     * La función get devuelve el elemento en la posición dada
     * si la posicion no es correcta dentro del rango 0 .. n-1
     * o la lista se encuentra vacía
     * está función retorna -1
     * @param pos
     * @return 
     */
    public Nodo get(int pos) {
        Nodo p = L;
        int i = 0;
        while(p != null) {
            if(i == pos) {
                return p;
            }
            p = p.link;
            i++;
        }
        return null;
    }
    
    public int size() {
        return n;
    }

    @Override
    public String toString() {
        String s = "[";
        Nodo p = L;
        while(p != null) {
            s = s +"\n"+"["+"\n";
            s = s + " Nombre="+p.nombre+"\n";
            s = s + " Formato="+p.formato+"\n";
            s = s + " Tamaño="+p.tamano+"\n";
            s = s + " Path="+p.path+"\n";            
            s = s + "]"+"\n";
            p = p.link;
        }        
        return s+"]";
    }
}
