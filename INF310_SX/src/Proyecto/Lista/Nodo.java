/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proyecto.Lista;

/**
 *
 * @author Ronaldo Rivero
 */
public class Nodo {
    public String nombre;
    public String formato;
    public int tamano;
    public String path;
    
    public Nodo link;

    public Nodo(String nombre, String formato, int tamano, String path) {
        this.nombre = nombre;
        this.formato = formato;
        this.tamano = tamano;
        this.path = path;
    }
    
    
}
