/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proyecto;

import static Proyecto.Main.PARSE;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ronaldo Rivero
 */
public class Comando {
   
    public static String DIR = "dir";
    public static String DEL = "del";
    public static String PAR = "\\";
    
   
    public static List<String> getDir(String path) {
        try {
            List<String> lines = new ArrayList<>();
            String app = "cmd /c " + DIR + " " + path;//cmd /c dir D:\\Prueba
            Process process = Runtime.getRuntime().exec(app);
            InputStream inputstream = process.getInputStream();
            BufferedReader input = new BufferedReader(new InputStreamReader(inputstream)); 
            
            while(input.read() > 0) {
                String row = input.readLine();  
                lines.add(row);
            }
            return lines;
        } catch (IOException ex) {
            return new ArrayList<>();
        }
    }
    
    public static void del(String path, String nombre, String formato) {        
        try {            
            String app = "cmd /c " + DEL + " /s /q " +'"'+ path + PAR + nombre + formato+'"';
            System.err.println(app);
            Process process = Runtime.getRuntime().exec(app);
            InputStream inputStream = process.getInputStream();
            BufferedReader input = new BufferedReader(new InputStreamReader(inputStream)); 
            
            while(input.read() > 0) {
                String row = input.readLine();  
                System.err.println(row);
            }
        } catch (IOException ex) {
            Logger.getLogger(Comando.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void delete(String path) {
        try {
            System.err.println(path);
            String app = "cmd /c " + DEL + " /s /q " + path;
            Process process = Runtime.getRuntime().exec(app);
            InputStream inputStream = process.getInputStream();
            BufferedReader input = new BufferedReader(new InputStreamReader(inputStream)); 
            
            while(input.read() > 0) {
                String row = input.readLine();  
                System.out.println(row);
            }
        } catch (IOException ex) {
            Logger.getLogger(Comando.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
