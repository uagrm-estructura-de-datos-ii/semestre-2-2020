/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ADT_BST;

import ADT_BST.Nodo;
import java.util.LinkedList;
import sun.misc.Queue;

/**
 *
 * @author ronal
 */
public class Arbol {
    private Nodo Raiz;

    public Arbol() {
    }
    
    public Arbol(int x) {
        Raiz = new Nodo(x);
    }
    
    public int cantLadders() {
        return cantLadders(Raiz);
    }
    
    private int cantLadders(Nodo p) {
        if(p == null)
            return 0;
        if(p.esHoja())
            return 0;
        
        int c = 0;
        if(isLadders(p)) {
            c = 1;
        }
        c = c + cantLadders(p.getHI());
        c = c + cantLadders(p.getHD());
        return c;
    }
     
    private boolean isLadders(Nodo p) {
        if(p.cantidadHijos() == 2){
            int d = p.getData();
            if(p.getHI().getData() == d-1 && p.getHD().getData() == d+1){
                return true;
            }
        }
        return false;
    }
    
    public int cantGajos() {
        return cantGajos(Raiz)-1;
    }
    
    private int cantGajos(Nodo p) {
        if(p == null)
            return 0;
        if(p.esHoja())
            return 1;
        
        int c = 1;
        c = c + cantGajos(p.getHI());
        c = c + cantGajos(p.getHD());
        return c;
    } 
    
    public void descendientes(int x) {
        descendientes(Raiz, x, false);
    }
    
    private void descendientes(Nodo p, int x, boolean b) {
        if(p == null)
            return;           
        if(b == true)
            System.out.print(p.getData() + " - ");       
        if(p.getData() == x)
            b = true;
        descendientes(p.getHI(), x, b);
        descendientes(p.getHD(), x, b);        
    }
    
    public boolean isPadre(int p, int h){
        return isPadre(Raiz, p, h);
    }
    
    public void cutLeafBunchs(int x) {
        if(Raiz == null)return;
        Nodo ant = null;
        Nodo aux = Raiz;
        while(aux != null) {
            //System.out.print(aux.getData() + " - ");
            if(x<aux.getData()){
                ant = aux;
                aux = aux.getHI();
            } else if(x > aux.getData()){
                ant = aux;
                aux = aux.getHD();
            } else {
                break;// si x es igual a aux.getData();
            }   
        }
        if(aux == null)return;//x no existe en el arbol

        System.out.println("Padre : "+ant.getData() + " Hijo : "+aux.getData());
        System.out.println("El padre " +ant.getData() + " es Racimo : " + esRacimo(ant));
        if(esRacimo(ant)){
            if(x < ant.getData())
                ant.setHI(null);
            else
                ant.setHD(null);
        }
    }
    
    private boolean esRacimo(Nodo p) {
        if(p.cantidadHijos() == 2) {
            if(p.getHI().esHoja() && p.getHD().esHoja())
                return true;
        }
        return false;
    }
    
    public void recorridoIterativo() {
        if(Raiz == null)
            return;
        LinkedList<Integer> nivel = new LinkedList<>();
        LinkedList<Nodo> cola = new LinkedList<>();
        cola.offer(Raiz);
        nivel.offer(1);
        int chojas = 0;
        while(cola.isEmpty() == false) {
            Nodo a = cola.poll();
            Integer n = nivel.poll();
            
            //accion
            System.out.println("Nivel "+n+ " : "+ a.getData());
            
            
            if(a.getHI()!= null){
                cola.offer(a.getHI());
                nivel.offer(n+1);
            }                
            if(a.getHD() != null){
                cola.offer(a.getHD());
                nivel.offer(n+1);
            }                
        }           
    }

    private boolean isPadre(Nodo nodo, int p, int h) {
        if(nodo == null)
            return false;
        if(nodo.esHoja())
            return false;
        
        if(nodo.getData() == p) {
            if(nodo.getHI()!= null && nodo.getHI().getData() == h) {
                return true;
            }
            if(nodo.getHD()!= null && nodo.getHD().getData() == h) {
                return true;
            }
        }
        boolean izq = isPadre(nodo.getHI(), p, h);
        if(izq == true)
            return true;
        return isPadre(nodo.getHD(), p, h);
    }
    
    public boolean isHoja(int x) {
        return isHoja(Raiz, x);
    }
    private boolean isHoja(Nodo p, int x) {
        if(p == null)
            return false;
        if(p.esHoja()) {
            if(p.getData() == x)
                return true;
            return false;
        }
        
        boolean izq = isHoja(p.getHI(), x);
        if(izq == true)
            return true;
        return isHoja(p.getHD(), x);
    }

    
    public void insertar(int x) {
        if(Raiz == null){
            Raiz = new Nodo(x);
            return;
        }
        Nodo aux1 = Raiz;
        Nodo aux2 = null;

        while(aux1 != null) {
            aux2 = aux1;
            if(x < aux1.getData()) {
                aux1 = aux1.getHI();
            } else if(x > aux1.getData()) {
                aux1 = aux1.getHD();
            } else {
                System.err.println("El elemento "+x+" ya éxiste en el árbol.");
                return;
            }
        }

        Nodo p = new Nodo(x);  
        if(x < aux2.getData()) {
            aux2.setHI(p);
        } else {
            aux2.setHD(p);
        }
    }
    
    public void inOrden() {
        inOrden(Raiz);
    }
    
    private void inOrden(Nodo p) {
        if(p == null)
            return;
        inOrden(p.getHI());
        System.out.print(p);
        inOrden(p.getHD());
    }
    
    public void podar(int x) {
       Raiz = podar1(Raiz, x);
    }
    
    private Nodo podar1(Nodo p, int x) {
        if(p == null)
            return null;
        
        if(p.esHoja())
            if(p.getData() == x)
                return null;
        
        if(p.getData() == x)
            return null;
        p.setHI( podar1(p.getHI(), x) );
        p.setHD( podar1(p.getHD(), x) );
        return p;
    }
    
    public void eliminar(int x) {
        Raiz = eliminar1(Raiz, x);
    }
    
    private Nodo eliminar1(Nodo p, int x) {
        if(p == null)
            return null;
        
        if(p.esHoja())
            if(p.getData() == x)
                return null;
        
        if(p.getData() == x) {
            int pre = prefijo(p);
            if(pre != -1) {
                p.setData(pre);
                p.setHI( eliminar1(p.getHI(), pre) );
                return p;
            } else {
                //aqui introduzca el postfijo
            }            
        }
        p.setHI( eliminar1(p.getHI(), x) );
        p.setHD( eliminar1(p.getHD(), x) );
        return p;
    }
    
    public int nivel() {
        return nivel1(Raiz);
    }
    
    private int nivel1(Nodo p) {
        if(p == null) 
            return 0;
        
        if(p.esHoja())
            return 1;
        
        int ni = nivel1(p.getHI());
        int nd = nivel1(p.getHD());
        return ni > nd ? ni + 1: nd + 1;        
    }
    
    public void podarNivel(int n) {
        Raiz = podarNivel1(Raiz, n);
    }
    
    private Nodo podarNivel1(Nodo p, int n) {
        if(p == null)
            return null;
        
        if(p.esHoja())
            if(n == 1)
                return null;
        
        if(n == 1)
            return null;
        p.setHI( podarNivel1(p.getHI(), n-1) );
        p.setHD( podarNivel1(p.getHD(), n-1) );
        return p;
    }
    
    public void eliminarNivel(int n) {
        Raiz = eliminarNivel1(Raiz, n);
    }
    
    private Nodo eliminarNivel1(Nodo p, int n) {
        if(p == null)
            return null;
        
        if(n == 1) {
            int pre = prefijo(p);
            if(pre != -1) {
                p.setData(pre);
                p.setHI( eliminar1(p.getHI(), pre) );
                return p;
            }
        }
        p.setHI(eliminarNivel1(p.getHI(), n-1));
        p.setHD(eliminarNivel1(p.getHD(), n-1));
        return p;
    }
    
    public void eliminarTriangulo() {
        Raiz = eliminarTriangulo1(Raiz);
    }
    
    private Nodo eliminarTriangulo1(Nodo p) {
        if(p == null)
            return null;
        
        if(p.cantidadHijos() == 2) {
            int sum = p.getData() + p.getHI().getData() + p.getHD().getData();           
            if(sum % 2 == 1) {
                int pre = prefijo(p);
                if(pre != -1) {
                    p.setData(pre);
                    p.setHI( eliminar1(p.getHI(), pre) );
                    
                    //return p; //es para que no continue analizando con sus hijos
                } else {
                    // aqui viene el postfijo
                }
            }
        }
        p.setHI( eliminarTriangulo1(p.getHI()) );
        p.setHD( eliminarTriangulo1(p.getHD()) );
        return p;
    }
    
    /**
     * devuelve el valor del prefijo, 
     * en caso el nodo sea nulo o hoja devuelve -1.
     * @param p
     * @return 
     */
    private int prefijo(Nodo p) {
        if(p == null || p .esHoja())
            return -1;
        
        Nodo aux = p.getHI();        
        while(aux.getHD() != null) {
            aux = aux.getHD();
        }        
        return aux.getData();
    }
}
