/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ADT_BST;

/**
 *
 * @author ronal
 */
public class Main {

    private static int data[] = {75, 50, 100, 25, 70, 10, 111, 80};
    
    public static void main(String[] args) {
        cutLeafBunchs();
        /*
        Arbol bst = new Arbol();
        load(bst);
        
        imprimir(bst, "MOSTRANDO INORDEN");        
        /*bst.podar(100);
        imprimir(bst, "DESPUES DE PODAR 100");
        
        bst.eliminar(75);
        imprimir(bst, "DESPUES DE ELIMINAR 75");
        
        System.out.println("Nivel : " + bst.nivel());
        
        bst.podarNivel(3);
        imprimir(bst, "DESPUES DE PODAR POR NIVEL 3");
        bst.eliminarNivel(3);
        imprimir(bst, "DESPUES DE ELIMINAR POR NIVEL 3");*/
        /*
        bst.eliminarTriangulo();
        imprimir(bst, "DESPUES DE ELIMINAR TRIANGULO IMPAR");
        */    
    }
    
    private static void load(Arbol bst) {
        for (int x : data) {
            bst.insertar(x);
        }
    }
    
    private static void imprimir(Arbol bst, String titulo) {
        System.out.println(titulo);
        bst.inOrden();
        System.out.println("\n");
    }
    
    private static void pregCantLadders() {
        Arbol bst = new Arbol();
        bst.insertar(30);
        bst.insertar(20);
        bst.insertar(60);
        bst.insertar(19);
        bst.insertar(21);
        bst.insertar(45);
        bst.insertar(70);
        bst.insertar(8);
        bst.insertar(22);
        bst.insertar(44);
        bst.insertar(46);
        bst.insertar(7);
        bst.insertar(9);
        
        System.out.println("Cantidad de Ladders : " + bst.cantLadders());
        
    }
    
    private static void pregCantGajos() {
        Arbol bst = new Arbol();
        bst.insertar(60);
        bst.insertar(30);
        bst.insertar(70);
        bst.insertar(10);
        bst.insertar(80);
        
        System.out.println("Cantidad de Gajos : " + bst.cantGajos());
    }
    
    private static void pregDescendiente() {
        Arbol bst = new Arbol();
        bst.insertar(60);
        bst.insertar(30);
        bst.insertar(70);
        bst.insertar(10);
        bst.insertar(80);
        
        bst.descendientes(80);
    }
    
    private static void pregIsPadre() {
        Arbol bst = new Arbol();
        bst.insertar(30);
        bst.insertar(20);
        bst.insertar(40);
        bst.insertar(10);
        bst.insertar(25);
        bst.insertar(50);
        
        System.out.println("30 es padre de 40 : " +bst.isPadre(30, 40));
        System.out.println("20 es padre de 50 : " +bst.isPadre(20, 50));
        System.out.println("20 es padre de 25 : " +bst.isPadre(20, 25));
    }
    
    private static void pregIsHoja() {
        Arbol bst = new Arbol();
        bst.insertar(30);
        bst.insertar(20);
        bst.insertar(40);
        bst.insertar(10);
        bst.insertar(25);
        bst.insertar(50);
        
        System.out.println("30 es hoja : " +bst.isHoja(30));
        System.out.println("10 es hoja : " +bst.isHoja(10));
        System.out.println("50 es hoja : " +bst.isHoja(50));
        System.out.println("80 es hoja : " +bst.isHoja(80));
    }
    
    private static void cutLeafBunchs() {
        Arbol bst = new Arbol();
        bst.insertar(60);
        bst.insertar(40);
        bst.insertar(65);
        bst.insertar(30);
        bst.insertar(50);
        bst.insertar(62);
        bst.insertar(70);
        bst.insertar(20);
        bst.insertar(35);
        bst.insertar(55);
        bst.insertar(68);
        bst.insertar(80);
        bst.inOrden();
        
        bst.cutLeafBunchs(35);
        bst.inOrden();
        //bst.recorridoIterativo();
    }
}
