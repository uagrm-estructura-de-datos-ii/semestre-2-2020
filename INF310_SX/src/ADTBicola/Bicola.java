/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ADTBicola;

/**
 *
 * @author Ronaldo Rivero
 */
public class Bicola {
    private Nodo L;
    
    public Bicola(){
        L = null;
    }
    
    public void add(int x) {
        if(L == null){
            L = new Nodo();
            L.data = x;
            L.link = L;
        } else {
            Nodo nuevo = new Nodo();
            nuevo.data = x;
            nuevo.link = L.link;
            L.link = nuevo;
            L = nuevo;
        }
    }
    
    public void addToFront(int x) {
        if(L == null){
            L = new Nodo();
            L.data = x;
            L.link = L;
        } else {
            Nodo nuevo = new Nodo();
            nuevo.data = x;
            nuevo.link = L.link;
            L.link = nuevo;
        }
    }
    
    public int pop() {
        if(L == null)
            return -1;
        // lista tiene un solo elemento;
        if(length() == 1) {
            int a = L.data;
            L = null;
            return a;
        }
        //cuando tiene mas de un elemento
        int a = L.link.data;
        L.link = L.link.link;
        return a;
    }
    
    public int popLast() {
        if(L == null)
            return -1;
        // lista tiene un solo elemento;
        if(length() == 1) {
            int a = L.data;
            L = null;
            return a;
        }
        //cuando tiene mas de un elemento
        int a = L.data;
        Nodo aux = L.link;
        while(aux.link.data != L.data) {
            aux = aux.link;
        }
        aux.link = L.link;
        L = aux;
        return a;
    }
    
    public int length(){
        if(L == null)
            return 0;
        int c = 0;
        Nodo aux = L.link;
        while(aux.data != L.data) {
            aux = aux.link;
            c++;
        }        
        return c+1;
    }
}
