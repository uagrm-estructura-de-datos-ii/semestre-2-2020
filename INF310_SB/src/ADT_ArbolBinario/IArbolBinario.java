/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ADT_ArbolBinario;

import java.util.List;

/**
 *
 * @author Ronaldo Rivero
 */
public interface IArbolBinario<K extends Comparable<K>,V>{
    
    public void insertar(K clavePadre, K claveHijo, V valorHijo) throws DatoYaExiste;

    public Nodo<K, V> buscar(Nodo<K,V> nodo, K clave);

    public void recorridoEnInOrden();
    
    public int cantidadHojas();
    
    public int cantidadHojasImp();
    
    public int cantidadHijoTriangulo();
}
