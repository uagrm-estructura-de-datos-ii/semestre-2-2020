/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ADT_ArbolBinario;

import java.util.List;

/**
 *
 * @author Ronaldo Rivero
 */
public class ArbolBinario<K extends Comparable<K>, V> implements IArbolBinario<K, V> {
    
    protected Nodo<K, V> raiz;
    protected int n;
    
    public ArbolBinario(K clave, V valor) {
        this.raiz = new Nodo<>(clave, valor);
        this.n = 1;
    }
    
    @Override
    public void insertar(K clavePadre, K claveHijo, V valorHijo) throws DatoYaExiste {
        Nodo<K, V> dirPadre = this.buscar(raiz, clavePadre);
        if(dirPadre != null) {
            Nodo<K, V> dirHijo = this.buscar(raiz, claveHijo);
            if(dirHijo == null) {
                int pos = dirPadre.posHijoLibre();
                if(pos != -1) {
                    dirPadre.setHijo(new Nodo(claveHijo, valorHijo), pos);
                    n++;
                } else {
                    System.out.println("El padre esta completo y no puede aceptar un nuevo hijo");
                }
            } else {                
                System.out.println("La Clave del hijo ya existe");
            }
        } else {
            System.out.println("La Clave del padre no existe");
        }                      
    }

    @Override
    public Nodo<K, V> buscar(Nodo<K,V> nodo, K clave) {
        if(nodo == null) {
            return null;
        }
        if(nodo.getClave() == clave) {
            return nodo;
        }
        Nodo<K,V> h1 = this.buscar(nodo.getHijo(1), clave);        
        if (h1 != null) {
            return h1;
        }
        return this.buscar(nodo.getHijo(2), clave);
    }

    @Override
    public void recorridoEnInOrden() {
        System.out.println("INORDEN");
        if(this.raiz == null) {
            System.out.println("Arbol vacio");
        } else {
            this.recorridoEnInOrden1(raiz);
        }
    }

    private void recorridoEnInOrden1(Nodo<K,V> nodo) {
        if(nodo != null) {
            this.recorridoEnInOrden1(nodo.getHijo(1));
            System.out.println("[ " + nodo.getClave() + " ]");
            this.recorridoEnInOrden1(nodo.getHijo(2));
        }
    }

    @Override
    public int cantidadHojas() {
        return cantidadHojas(raiz);
    }
    
    private int cantidadHojas(Nodo<K,V> nodo) {
        if(nodo == null)
            return 0;
        if(nodo.cantidadHijo() == 0)
            return 1;
        return cantidadHojas(nodo.getHijo(1)) + cantidadHojas(nodo.getHijo(2));
    }
    
    @Override
    public int cantidadHojasImp() {
        return cantidadHojasImp(raiz);
    }
    
    private int cantidadHojasImp(Nodo<K,V> nodo) {
        if(nodo == null)
            return 0;
        if(nodo.cantidadHijo() == 0){
            if( Integer.parseInt(String.valueOf(nodo.getClave())) % 2 == 1)
                return 1;
            return 0;                      
        }            
        return cantidadHojasImp(nodo.getHijo(1)) + cantidadHojasImp(nodo.getHijo(2));
    }

    @Override
    public int cantidadHijoTriangulo() {
        return cantidadHijoTriangulo(raiz);
    }
    
    private int cantidadHijoTriangulo(Nodo<K,V> nodo) {
        if(nodo == null)
            return 0;
        if(nodo.cantidadHijo() == 0)
            return 0;
        if(nodo.cantidadHijo() == 2) {
            if(nodo.getHijo(1).cantidadHijo() == 0 && nodo.getHijo(2).cantidadHijo() == 0)
                return 1;
        }
        
        return cantidadHijoTriangulo(nodo.getHijo(1)) + cantidadHijoTriangulo(nodo.getHijo(2));
    }
}
