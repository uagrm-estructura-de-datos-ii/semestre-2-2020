/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ADT_ArbolBinario;

/**
 *
 * @author Ronaldo Rivero
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)throws Exception {        
        ArbolBinario<Integer, String> arbolb = new ArbolBinario<>(50, "Hola Mundo");
        arbolb.insertar(50, 25, "Adios Mundo");
        arbolb.insertar(50, 10, "Adios Mundo");
        arbolb.insertar(25, 29, "Adios Mundo");
        arbolb.insertar(55, 10, "Adios Mundo");
        arbolb.insertar(50, 15, "Adios Mundo");
        arbolb.insertar(25, 100, "Adios Mundo");
        arbolb.insertar(10, 55, "Adios Mundo");
        arbolb.insertar(10, 75, "Adios Mundo");
        arbolb.recorridoEnInOrden();        
        System.out.println("Cantidad de Hojas : " + arbolb.cantidadHojas());
        System.out.println("Cantidad de Hojas Impares : " + arbolb.cantidadHojasImp());
        System.out.println("Cantidad de Hijo Triangulo : " + arbolb.cantidadHijoTriangulo());
    }
    
}
