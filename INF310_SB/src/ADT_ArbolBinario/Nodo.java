/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ADT_ArbolBinario;

/**
 *
 * @author Ronaldo Rivero
 */
public class Nodo<K, V> {
    private K clave;
    private V valor;
    private Nodo<K, V> h1;
    private Nodo<K, V> h2;

    public Nodo() {
    }

    public Nodo(K clave, V valor) {
        this.clave = clave;
        this.valor = valor;
    }

    public K getClave() {
        return clave;
    }

    public void setClave(K clave) {
        this.clave = clave;
    }

    public V getValor() {
        return valor;
    }

    public void setValor(V valor) {
        this.valor = valor;
    }

    public void setHijo(Nodo<K, V> hijo, int n) {
        if(n == 1) {
            this.h1 = hijo;
        }else if(n == 2) {
            this.h2 = hijo;
        } else {
            System.out.println("El indice de hijo que desea settear es erroneo.");
        }
    }
    
    public Nodo<K, V> getHijo(int n) {
        if(n == 1) {
            return this.h1;
        }else if(n == 2) {
            return this.h2;
        } else {
            System.out.println("El indice de hijo que desea settear es erroneo.");
            return null;
        }
    }
    
    public int cantidadHijo() {
        int a = 0;
        if(this.h1 != null) {
            a++;
        }
        if(this.h2 != null) {
            a++;
        }
        return a;
    }
    
    public int posHijoLibre() {
        if(this.h1 == null) {
            return 1;
        }
        if(this.h2 == null) {
            return 2;
        }
        return -1;
    }

    @Override
    public String toString() {
        return "" + clave;
    }
}
