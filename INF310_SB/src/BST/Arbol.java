/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BST;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 *
 * @author Ronaldo Rivero
 */
public class Arbol<K extends Comparable<K>, V> implements IArbol<K, V> {
     
    protected Nodo<K, V> raiz;
    protected int n = 0;

    public boolean esVacio() {
        return Nodo.esNodoVacio(raiz);       
    }
    
    @Override
    public void insertar(K clave, V valor) throws DatoYaExiste {
        //clave es lo mismo X
        //El arbol esta vacio?
        if(esVacio()){ //Si esta vacio, lo construimo
            this.raiz = new Nodo(clave, valor);
        }else{
            Nodo<K, V> azul = null;
            Nodo<K, V> rojo = this.raiz;
            while(rojo != null) {
                azul = rojo;           
                //clave < rojo.getClave()
                if(clave.compareTo(rojo.getClave()) < 0)
                    rojo = rojo.getHijoIzquierdo();                   
                else{
                    //clave > rojo.getClave()
                    if(clave.compareTo(rojo.getClave()) > 0)
                        rojo = rojo.getHijoDerecho();
                    else{ 
                        //clave == rojo.getClave()
                        throw new DatoYaExiste("La clave ya existe " + clave);                        
                    }
                }
            }
            if (clave.compareTo(azul.getClave()) < 0){
                azul.setHijoIzquierdo(new Nodo(clave, valor));                
            }else{
                azul.setHijoDerecho(new Nodo(clave, valor));
            }
        }   
        this.n++;
    }

    @Override
    public void inOrden() {
        System.out.println("IN-ORDEN");
        if(esVacio()) {
            System.out.println("El arbol esta vacio");
        } else {
            inOrden1(raiz);
        }
    }
        
    private void inOrden1(Nodo<K, V> nodo) {
        if(!Nodo.esNodoVacio(nodo)){
            inOrden1(nodo.getHijoIzquierdo());
            System.out.println("[ "+ nodo.getClave()+"|"+ nodo.getValor()+ "]");
            inOrden1(nodo.getHijoDerecho());
        }      
    }              

    @Override
    public void podar(K x) {
        this.raiz = podar1(this.raiz, x);
    }
    
    private Nodo podar1(Nodo<K, V> nodo, K x){
        if (nodo == null){
            return null;
        }
        if(nodo.esHoja()) {
            if (x.compareTo(nodo.getClave()) == 0) {
                return null;
            }
            return nodo;
        }
            
        if(x.compareTo(nodo.getClave()) == 0) {
            return null;
        }
        nodo.setHijoIzquierdo(podar1(nodo.getHijoIzquierdo(), x));
        nodo.setHijoDerecho(podar1(nodo.getHijoDerecho(), x));
        return nodo;
    }
    
    private Nodo<K, V> prefijo(Nodo<K, V> nodo) {
        Nodo<K, V> ant = nodo;
        Nodo<K, V> act = nodo.getHijoIzquierdo();
        while(act != null) {
            ant = act;
            act = act.getHijoDerecho();
        }
        
        return ant;
    } 
    private Nodo<K, V> posfijo(Nodo<K, V> nodo) {
        Nodo<K, V> ant = nodo;
        Nodo<K, V> act = nodo.getHijoDerecho();
        while(act != null) {
            ant = act;
            act = act.getHijoIzquierdo();
        }
        
        return ant;
    } 

    @Override
    public void eliminar(K x) {
        this.raiz = eliminar1(this.raiz, x);
    }
    
    private Nodo<K, V> eliminar1(Nodo<K, V> nodo, K x) {
        if(nodo == null)
            return null;
        if(nodo.esHoja()) {
            if(x.compareTo(nodo.getClave()) == 0)
                return null;
            return nodo;
        }
        
        if(x.compareTo(nodo.getClave()) == 0) {
            if(nodo.getHijoIzquierdo() != null) {
                Nodo<K, V> aux = prefijo(nodo);
                nodo.setClave(aux.getClave());
                nodo.setValor(aux.getValor());
                
                nodo.setHijoIzquierdo(eliminar1(nodo.getHijoIzquierdo(), aux.getClave()));                
            } else {
                Nodo<K, V> aux = posfijo(nodo);
                nodo.setClave(aux.getClave());
                nodo.setValor(aux.getValor());
                
                nodo.setHijoDerecho(eliminar1(nodo.getHijoDerecho(), aux.getClave())); 
            }
            return nodo;
        }
        
        nodo.setHijoIzquierdo( eliminar1(nodo.getHijoIzquierdo(), x) );
        nodo.setHijoDerecho(eliminar1(nodo.getHijoDerecho(), x) );
        return nodo;
    }

    @Override
    public void eliminarIncompleto(K x) {
        raiz = eliminarIncompleto(raiz, x);
    }
    
    private Nodo eliminarIncompleto(Nodo<K, V> nodo, K x) {
        if(nodo == null)
            return null;
        if(nodo.esHoja())
            return nodo;
        if(x.compareTo(nodo.getClave()) == 0){
            if(nodo.cantidadHijos() == 1) {
                if(nodo.getHijoIzquierdo() != null) {
                    nodo = nodo.getHijoIzquierdo();
                } else {
                    nodo = nodo.getHijoDerecho();
                }
                return nodo;
            }
        }
        if(x.compareTo(nodo.getClave()) > 0) {
            nodo.setHijoDerecho( eliminarIncompleto(nodo.getHijoDerecho(), x) );
        }
        if(x.compareTo(nodo.getClave()) < 0) {
            nodo.setHijoIzquierdo(eliminarIncompleto(nodo.getHijoIzquierdo(), x) );
        }
        return nodo;
    }

    @Override
    public boolean similar(Arbol<K, V> A) {
        return similar(raiz, A.raiz);
    }
    
    private boolean similar(Nodo<K, V> self, Nodo<K, V> a) {
        //es null
        if(self == null) {
            if(a == null) {
                return true;
            }
            return false;
        }        
        //es hoja
        if(self.esHoja()) {
            if(a.esHoja()) {
                return true;
            }
            return false;
        }
        //general
        if(self.cantidadHijos() != a.cantidadHijos()) {
            return false;
        }
        //YO padre soy igual al otro padre
        //pero no se si mi hijo izq es igual al hijo izq del otro
        boolean s1 = similar(self.getHijoIzquierdo(), a.getHijoIzquierdo());
        //pero tampoco se si mi hijo der es igual al hijo der del otro
        boolean s2 = similar(self.getHijoDerecho(), a.getHijoDerecho());
        if(s1 == false || s2 == false) {
            return false;
        }
        return true;
    }

    @Override
    public int altura() {
        return altura(this.raiz);   
    }
      
    private int altura(Nodo<K,V> nodoActual) {
      if(Nodo.esNodoVacio(nodoActual)){ // como decir que n = 0
          return 0;   
      }
      int alturaPorIzqui = altura(nodoActual.getHijoIzquierdo());
      int alturaPorDerecha = altura(nodoActual.getHijoDerecho());
      return alturaPorIzqui > alturaPorDerecha ? alturaPorIzqui + 1 : alturaPorDerecha + 1;
    }
    
    private int cantidadNodos(Nodo<K, V> nodo) {
        if(nodo == null)
            return 0;
        if(nodo.esHoja())
            return 1;
        return cantidadNodos(nodo.getHijoIzquierdo()) 
                + cantidadNodos(nodo.getHijoDerecho()) + 1;
    }

    
    @Override
    public boolean lleno() {
        int h = altura(this.raiz);
        int ch = (int)Math.pow(2, h) - 1;
        return ch == cantidadNodos(this.raiz);
    }
    
    
    public boolean llenoR() {
        if(raiz != null)
            return llenoR(raiz.getHijoIzquierdo(), raiz.getHijoDerecho());
        return false;
    }
    
    private boolean llenoR(Nodo<K,V> nodo1, Nodo<K, V> nodo2) {
        if(nodo1 == null) {
            if(nodo2 == null)
                return true;
            return false;
        }        
        if(nodo1.esHoja()) {
            if(nodo2.esHoja()) {
                return true;
            }
            return false;
        }
        if(!(nodo1.cantidadHijos() == 2 && nodo2.cantidadHijos() == 2)) {
            return false;
        }
        boolean h1 = llenoR(nodo1.getHijoIzquierdo(), nodo1.getHijoDerecho());
        boolean h2 = llenoR(nodo2.getHijoIzquierdo(), nodo2.getHijoDerecho());
        if(h1 == false || h2 == false) {
            return false;
        }
        return true;        
    }
    
    public Arbol<K, V> invertir() {
        Arbol<K, V> nuevo = new Arbol<>();
        nuevo.raiz = invertir(this.raiz);
        //
        return nuevo;
    }
    
    private Nodo<K, V> invertir(Nodo<K, V> nodo){
        if(nodo == null)
            return null;
        if(nodo.esHoja())
            return nodo;
        
        Nodo<K, V> HI = nodo.getHijoIzquierdo();
        nodo.setHijoIzquierdo( nodo.getHijoDerecho() );
        nodo.setHijoDerecho( HI );
        
        nodo.setHijoIzquierdo( invertir(nodo.getHijoIzquierdo()) );
        nodo.setHijoDerecho( invertir(nodo.getHijoDerecho()) );
        
        return nodo;
    }

    public void recorridoIterativo() {
        Queue<Integer> colaDeNiveles = new LinkedList<>();
        Queue<Nodo<K, V>> colaDeNodos = new LinkedList<>();        
        if (this.raiz == null) {
            return;
        }        
        colaDeNodos.add(this.raiz);
        colaDeNiveles.add(0);        
        while(!colaDeNodos.isEmpty()) {
            Nodo<K, V> nodo = colaDeNodos.poll();
            Integer nivel = colaDeNiveles.poll();
            System.out.println(" Nivel : "+nivel + "   KEY : " + nodo.getClave());
            //negocio
            if(nodo.getHijoIzquierdo() != null) {
                colaDeNodos.add(nodo.getHijoIzquierdo());
                colaDeNiveles.add(nivel+1);
            }
            if(nodo.getHijoDerecho() != null) {
                colaDeNodos.add(nodo.getHijoDerecho());
                colaDeNiveles.add(nivel+1);
            }
        }
    }

    public int cantidadNodoSoloHijoIzquierdo() {
        Queue<Integer> colaDeNiveles = new LinkedList<>();
        Queue<Nodo<K, V>> colaDeNodos = new LinkedList<>();        
        if (this.raiz == null) {
            return 0;
        }        
        colaDeNodos.add(this.raiz);
        colaDeNiveles.add(0);   
        int cant = 0;
        while(!colaDeNodos.isEmpty()) {
            Nodo<K, V> nodo = colaDeNodos.poll();
            Integer nivel = colaDeNiveles.poll();           
            //begin::negocio
            if(nodo.cantidadHijos() == 1) {
                if(nodo.getHijoIzquierdo() != null){
                    cant++;
                }
            }            
            //end::negocio
            if(nodo.getHijoIzquierdo() != null) {
                colaDeNodos.add(nodo.getHijoIzquierdo());
                colaDeNiveles.add(nivel+1);
            }
            if(nodo.getHijoDerecho() != null) {
                colaDeNodos.add(nodo.getHijoDerecho());
                colaDeNiveles.add(nivel+1);
            }
        }
        return cant;
    }
    
    public int cantidadNodoSoloHijoIzquierdoN(int n) {
        Queue<Integer> colaDeNiveles = new LinkedList<>();
        Queue<Nodo<K, V>> colaDeNodos = new LinkedList<>();        
        if (this.raiz == null) {
            return 0;
        }        
        colaDeNodos.add(this.raiz);
        colaDeNiveles.add(0);   
        int cant = 0;
        while(!colaDeNodos.isEmpty()) {
            Nodo<K, V> nodo = colaDeNodos.poll();
            Integer nivel = colaDeNiveles.poll();           
            //begin::negocio
            if(nodo.cantidadHijos() == 1) {
                if(nodo.getHijoIzquierdo() != null){
                    if(nivel > n){
                        cant++;
                    }                    
                }
            }            
            //end::negocio
            if(nodo.getHijoIzquierdo() != null) {
                colaDeNodos.add(nodo.getHijoIzquierdo());
                colaDeNiveles.add(nivel+1);
            }
            if(nodo.getHijoDerecho() != null) {
                colaDeNodos.add(nodo.getHijoDerecho());
                colaDeNiveles.add(nivel+1);
            }
        }
        return cant;
    }

    private Nodo<K, V> rotacionSimpleIzq(Nodo<K, V> nodo) {
        Nodo<K, V> padre = nodo.getHijoIzquierdo();
        nodo.setHijoIzquierdo(padre.getHijoDerecho());
        padre.setHijoDerecho(nodo);        
        return padre;
    }
    
    private Nodo<K, V> rotacionSimpleDer(Nodo<K, V> nodo) {
        Nodo<K, V> padre = nodo.getHijoDerecho();
        nodo.setHijoDerecho(padre.getHijoIzquierdo());
        padre.setHijoIzquierdo(nodo);
        return padre;
    }
    
    private Nodo<K, V> rotacionDobleIzq(Nodo<K, V> nodo) {
        nodo.setHijoIzquierdo( rotacionSimpleDer(nodo.getHijoIzquierdo()) );
        return rotacionSimpleIzq(nodo);
    }
    
    private Nodo<K, V> rotacionDobleDer(Nodo<K, V> nodo) {
        nodo.setHijoDerecho(rotacionSimpleIzq(nodo.getHijoDerecho()) );
        return rotacionSimpleDer(nodo);
    }
    
    public void balancear() {
        this.raiz = balancear(this.raiz);
    }
    
    private Nodo<K, V> balancear(Nodo<K, V> nodo) {
        if(nodo == null)
            return null;
        if(nodo.esHoja())
            return nodo;
        nodo.setHijoIzquierdo(balancear(nodo.getHijoIzquierdo()));
        nodo.setHijoDerecho(balancear(nodo.getHijoDerecho()));
        
        int ai = altura(nodo.getHijoIzquierdo());
        int ad = altura(nodo.getHijoDerecho());        
            
        if(ai-ad >= 2){
            if(ad == 0) {
                nodo = rotacionSimpleIzq(nodo);
            } else {
                nodo = rotacionDobleIzq(nodo);
            }
            nodo.setHijoIzquierdo(balancear(nodo.getHijoIzquierdo()));
            nodo.setHijoDerecho(balancear(nodo.getHijoDerecho()));
        } else if(ai-ad <= -2) {
            if(ai == 0) {
                nodo = rotacionSimpleDer(nodo);
            } else {
                nodo = rotacionDobleDer(nodo);
            }
            nodo.setHijoIzquierdo(balancear(nodo.getHijoIzquierdo()));
            nodo.setHijoDerecho(balancear(nodo.getHijoDerecho()));
        }
        
        return nodo;
    }
}
