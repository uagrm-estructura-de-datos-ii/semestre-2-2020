/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BST;

/**
 *
 * @author Ronaldo Rivero
 */
public interface IArbol<K extends Comparable<K>,V> {
    
    void insertar(K clave, V valor) throws DatoYaExiste;    
    
    void inOrden();
    
    void podar(K x);
    
    void eliminar(K x);
    
    void eliminarIncompleto(K x);
    
    boolean similar(Arbol<K, V> A);
    
    int altura();
    
    boolean lleno();
}
