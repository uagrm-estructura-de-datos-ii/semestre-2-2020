/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BST;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ronaldo Rivero
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Arbol<Integer, String> arbol = new Arbol<>();;
        try {
            arbol.insertar(50, "50");
            arbol.insertar(25, "25");
            arbol.insertar(75, "75");
            arbol.insertar(10, "10");
            arbol.insertar(29, "29");
            arbol.insertar(27, "27");
            arbol.insertar(10, "10");
        } catch (DatoYaExiste ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }  
        arbol.inOrden();
    }
    
}
