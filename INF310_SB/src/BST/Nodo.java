/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BST;

/**
 *
 * @author Ronaldo Rivero
 */
public class Nodo<K, V>  {
    private K clave;
    private V valor;
    private Nodo<K, V> hijoIzquierdo;
    private Nodo<K, V> hijoDerecho;

    public Nodo() {
    }

    public Nodo(K clave, V valor) {
        this.clave = clave;
        this.valor = valor;
    }

    public K getClave() {
        return clave;
    }

    public void setClave(K clave) {
        this.clave = clave;
    }

    public V getValor() {
        return valor;
    }

    public void setValor(V valor) {
        this.valor = valor;
    }

    public Nodo<K, V> getHijoIzquierdo() {
        return hijoIzquierdo;
    }

    public void setHijoIzquierdo(Nodo<K, V> hijoIzquierdo) {
        this.hijoIzquierdo = hijoIzquierdo;
    }

    public Nodo<K, V> getHijoDerecho() {
        return hijoDerecho;
    }

    public void setHijoDerecho(Nodo<K, V> hijoDerecho) {
        this.hijoDerecho = hijoDerecho;
    }

    public boolean esVacioHijoIzquierdo() {
        return Nodo.esNodoVacio(this.getHijoIzquierdo());
    }

    public boolean esVacioHijoDerecho() {
        return Nodo.esNodoVacio(this.getHijoDerecho());
    }

    public boolean esHoja() {
        return this.esVacioHijoIzquierdo() && this.esVacioHijoDerecho();
    }
    
    public int cantidadHijos() {
        int i = 0;
        if(hijoIzquierdo != null)
            i++;
        if(hijoDerecho != null)
            i++;
        return i;
    }

    public static boolean esNodoVacio(Nodo nodo) {
        return nodo == null;
    }

    public static Nodo nodoVacio() {
        return null;
    }
    

    @Override
    public String toString() {
        return "" + clave;
    }

    boolean esNodoCompleto() {
        return (!esVacioHijoIzquierdo() && !esVacioHijoDerecho()); //To change body of generated methods, choose Tools | Templates.
    }
}
