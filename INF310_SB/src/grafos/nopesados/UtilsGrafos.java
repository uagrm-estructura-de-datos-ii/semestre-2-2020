package grafos.nopesados;

public class UtilsGrafos {
    public static boolean esConexo(Grafo unGrafo) {
        DFS dfsGrafo = new DFS(unGrafo, 0);
        return dfsGrafo.hayCaminoATodos();
    }

}
