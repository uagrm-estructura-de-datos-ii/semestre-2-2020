/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVias;

import java.util.List;
import java.util.Scanner;

/**
 *
 * @author hp
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int datos[] = {50,70, 30,40, 55, 65, 80, 90, 100};
        MVias arbol = new MVias();
        for (int dato : datos) {
            arbol.insertar(dato, "X");
        }
        
        System.out.println(arbol.recorridoEnInOrden());
        System.out.println(arbol.contarDatosNoVaciosEnNivel(0));
        System.out.println(arbol.cantidadDeHojasEnUnNivel(1));
        arbol.recorridoIterativo();
    }
}
